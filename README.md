# NAME

sh2\_mp\_ai\_enabler - Enables Multiplayer AI in Stronghold 2 Steam Edition, now you can play cooperatively with your friends against the AI.

## DEPENDENCIES

No special dependencies, regular POSIX libraries for Linux

## DESCRIPTION

**Only the game host should use this tool. Other players joining the game should not be running this tool.**

Launch this program first, then just start up Stronghold 2 from Steam. You can now host a multiplayer game and add AI opponents.

Official download page available at: https://www.questforgaming.com/stronghold2

The actual AI in the game is not modified in any way, this program just makes it possible to add the existing single player AI to multiplayer.

### HOW IT WORKS

This program has been updated to work a little different from previous versions.

When you host a multiplayer game, Stronghold 2 overwrites a variable in memory to 0. This variable controls if you can see the button to add AI opponents. When you leave the multiplayer lobby, the game changes this variable again to be 1. Previous versions of this program would change this variable to 1 every second. This ensured that any changes made by the game were quickly overwritten, and you could add AI.

The current version of this program now works by overwriting this instruction in memory. The whole instruction, which is 7 bytes long, is overwritten with 7 NOP codes (0x90). In addition, this program also does the same for the button that lets you add randomized AI opponents. These changes ensure that the game will no longer change the variable to 0. This means that the option to add AI will always be present, both in single player Kingmaker as well as multiplayer.

No game files are modified by this program, so there are no permanent changes.

## BUILDING

As with any compilation, you will need a toolchain properly set up. On Linux, just check with your distributions documentation. For Windows, my personal recommendation will always be MSYS2.

There is a Makefile available for easy building on both Linux and MSYS2 on Windows.

You can also use Visual Studio to compile. I have never tested it myself, but others have been helping with feedback to weed out build errors and warnings.

On Windows you don't need to include the gnu\_linux\_funcs.c source when compiling.

On Linux you don't need to include the ms\_funcs.c source when compiling. However, you do need to compile with the macro STRONGHOLD\_ON\_GNU\_LINUX defined.

### BUILDING WITH MAKEFILE

On Linux, all you need to do is 'cd' into the src directory and run 'make'. This will produce the binary 'sh2\_mp\_ai\_enabler'

On Windows in MSYS2, the procedure is almost the same. 'cd' into the src directory and run 'make windows'. This will produce the binary 'sh2\_mp\_ai\_enabler.exe'

You can provide additional CFLAGS and LDFLAGS if desired. Example: `make CFLAGS="-O2"`

## AUTHOR

Daniel Jenssen <daerandin@gmail.com>

