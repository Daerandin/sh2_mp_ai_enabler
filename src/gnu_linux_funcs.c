/****************************************************************************
*                                                                           *
*  sh2_mp_ai_enabler - Enable AI in Multiplayer lobby for Stronghold 2      *
*  Copyright (C) 2022  Daniel Jenssen <daerandin@gmail.com>            *
*                                                                           *
*  This program is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published by     *
*  the Free Software Foundation, either version 3 of the License, or        *
*  (at your option) any later version.                                      *
*                                                                           *
*  This program is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
*  GNU General Public License for more details.                             *
*                                                                           *
*  You should have received a copy of the GNU General Public License        *
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.   *
*                                                                           *
*****************************************************************************/

/* This file has all GNU/LINUX specific code that is used by the os_wrapper
 * source file. */

#define V_BYTES 7

#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <sys/ptrace.h>
#include "gnu_linux_funcs.h"
#include "os_wrapper.h"

void
write_into_memory(long shpid, long aval, unsigned long long new_opcode)
{
    int fd;
    char proc[] = "/proc/";
    char mem[] = "/mem";
    char path[44] = {'\0'};
    char strpid[32] = {'\0'};
    snprintf(strpid, 32, "%ld", shpid);
    strcpy(path, proc);
    strcat(path, strpid);
    strcat(path, mem);
    fd = open(path, O_WRONLY);
    ptrace(PTRACE_SEIZE, shpid, 0, 0);
    if (pwrite(fd, &new_opcode, V_BYTES, aval) != V_BYTES) {
        ptrace(PTRACE_DETACH, shpid, 0, 0);
        close(fd);
        printf("Error: Could not write into memory of PID: %ld\n", shpid);
        exit(EXIT_FAILURE);
    }
    if (pwrite(fd, &new_opcode, V_BYTES, aval+7) != V_BYTES) {
        ptrace(PTRACE_DETACH, shpid, 0, 0);
        close(fd);
        printf("Error: Could not write into memory of PID: %ld\n", shpid);
        exit(EXIT_FAILURE);
    }
    ptrace(PTRACE_DETACH, shpid, 0, 0);
    close(fd);
}

long
get_base_address(long shpid)
{
    char buffer[32] = {'\0'};
    FILE *sp;
    char strpid[32] = {'\0'};
    char path[44] = {'\0'};
    char bname[] = MODULE_NAME;
    int c, mc, bcounter = 0;
    bool readmem = true;
    mc = 0;

    snprintf(strpid, 32, "%ld", shpid);
    strcpy(path, "/proc/");
    strcat(path, strpid);
    strcat(path, "/maps");

    sp = fopen(path, "r");

    /* Now we read the memory map manually, bit by bit. */
    while ((c = fgetc(sp)) != EOF) {
        if (readmem && c == (int)'-') {
            readmem = false;
            buffer[bcounter] = '\0';
            bcounter = 0;
        } else if (readmem && c != (int)'-' && c != (int)'\n')
            buffer[bcounter++] = (char)c;
        else if (!readmem && c != (int)'\n') {
            if (c == bname[mc])
                mc++;
            else if (c != bname[mc] && mc)
                mc = 0;
        } else if (c == (int)'\n') {
            if (!readmem && (size_t)mc == strlen(bname)) {
                /* We got a match!. */
                fclose(sp);
                return strtol(buffer, NULL, 16);
            }
            readmem = true;
            mc = 0;
            bcounter = 0;
        }
    }
    /* If we get here, we didn't find a match. Time to call quits. */
    printf("Could not find base module address.\n");
    fclose(sp);
    return 0;
}

long
find_stronghold_pid(const char *pname)
{
    struct dirent *cu = NULL;
    struct stat buf;
    const char proc[] = "/proc/";
    const char comm[] = "/comm";
    char buffer[100] = {'\0'};
    char *eptr;
    DIR *dp;
    FILE *sp;
    long ppid;
    int c, i;

    if (!(dp = opendir(proc))) {
        printf("Error while attempting to find Stronghol 2 pid.\n");
        exit(EXIT_FAILURE);
    }

    /* Infinite loop, only break on error. */
    while (1) {
        errno = 0;
        cu = readdir(dp);
        if (errno)
            break;
        if (!cu) {
            closedir(dp);
            sleep(3);
            if (!(dp = opendir(proc)))
                break;
            continue;
        }
        if (cu->d_type == DT_UNKNOWN) {
            strcpy(buffer, proc);
            strcat(buffer, cu->d_name);
            if (lstat(buffer, &buf) == -1)
                break;
        }
        if (cu->d_type == DT_DIR ||
            (cu->d_type == DT_UNKNOWN && S_ISDIR(buf.st_mode))) {
            eptr = cu->d_name;
            errno = 0;
            ppid = strtol(cu->d_name, &eptr, 10);
            if (errno != 0 || *eptr != '\0')
                continue;
            strcpy(buffer, proc);
            strcat(buffer, cu->d_name);
            strcat(buffer, comm);
            /* We will simply skip this directory on an error. */
            if (!(sp = fopen(buffer, "r")))
                continue;
            i = 0;
            while ((c = fgetc(sp)) != EOF) {
                if (pname[i] != '\0' && pname[i] == (char)c)
                    i++;
                else
                    break;
            }
            fclose(sp);
            if (pname[i] == '\0') {
                closedir(dp);
                sleep(3);
                return ppid;
            }
        }
    }
    /* If we make it here, something™ went wrong. */
    printf("Error while attempting to find Stronghold 2 pid.\n");
    if (dp)
        closedir(dp);
    return 0;
}
