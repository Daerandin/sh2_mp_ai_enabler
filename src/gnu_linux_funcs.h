#ifndef GNU_LINUX_FUNCS_H
#define GNU_LINUX_FUNCS_H

void write_into_memory(long, long, unsigned long long);
long find_stronghold_pid(const char *);
long get_base_address(long);

#endif /* GNU_LINUX_FUNCS_H */
