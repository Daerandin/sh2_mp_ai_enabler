/****************************************************************************
*                                                                           *
*  sh2_mp_ai_enabler - Enable AI in Multiplayer lobby for Stronghold 2      *
*  Copyright (C) 2022  Daniel Jenssen <daerandin@gmail.com>            *
*                                                                           *
*  This program is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published by     *
*  the Free Software Foundation, either version 3 of the License, or        *
*  (at your option) any later version.                                      *
*                                                                           *
*  This program is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
*  GNU General Public License for more details.                             *
*                                                                           *
*  You should have received a copy of the GNU General Public License        *
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.   *
*                                                                           *
*****************************************************************************/

#define VERSION "0.3.1"

#include <stdio.h>
#include <stdlib.h>
#include "os_wrapper.h"

int
main(void)
{
    long shpid = 0;
    long aval = 0;

    printf("Stronghold 2 Multiplayer AI Enabler\nVersion %s\n\n", VERSION);
    printf("Starting to look for Stronghold 2 process...");
    fflush(stdout);
    shpid = getshpid();
    if (!shpid)
        return EXIT_FAILURE;
    printf(" found PID: %ld\n", shpid);

    printf("\nFinding address for instruction that disables AI.\n");
    aval = get_opcode_address(shpid);
    printf("Found memory address: %lx\n", aval);
    printf("Overwriting instruction that disables AI.\n");
    overwrite_opcode(shpid, aval);
    printf("Successful, AI will now remain enabled.\n");

    return EXIT_SUCCESS;
}
