#ifndef MS_FUNCS_H
#define MS_FUNCS_H

void write_into_memory(long, long, unsigned long long);
long find_stronghold_pid(const char *);
long get_base_address(long);

#endif /* MS_FUNCS_H */
