/****************************************************************************
*                                                                           *
*  sh2_mp_ai_enabler - Enable AI in Multiplayer lobby for Stronghold 2      *
*  Copyright (C) 2022  Daniel Jenssen <daerandin@gmail.com>            *
*                                                                           *
*  This program is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published by     *
*  the Free Software Foundation, either version 3 of the License, or        *
*  (at your option) any later version.                                      *
*                                                                           *
*  This program is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
*  GNU General Public License for more details.                             *
*                                                                           *
*  You should have received a copy of the GNU General Public License        *
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.   *
*                                                                           *
*****************************************************************************/

/* This file contains wrapper functions that perform calls to OS specific
 * functions in other source files. */

#ifdef STRONGHOLD_ON_GNU_LINUX
#include "gnu_linux_funcs.h"
#else
#include "ms_funcs.h"
#endif /* STRONGHOLD_ON_GNU_LINUX */

#include "os_wrapper.h"

/* This will find automatically find the PID of the game.
 * The actual work is handled by the OS specific functions.
 * If Stronghold 2 is not currently running, then this function will simply
 * wait and try again every 3 seconds. */
long
getshpid(void)
{
    return find_stronghold_pid(MODULE_NAME);
}

/* This will get the base address of the game, and add the offset to it. */
long
get_opcode_address(long shpid)
{
    return get_base_address(shpid) + POINTER_OFFSET;
}

/* Here we will call OS specific functions to write the new opcode into
 * the specified address. */
void
overwrite_opcode(long shpid, long address)
{
    write_into_memory(shpid, address, NEW_OPCODE);
}
