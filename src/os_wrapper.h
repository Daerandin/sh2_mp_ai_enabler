#ifndef OS_WRAPPER_H
#define OS_WRAPPER_H

#define MODULE_NAME "Stronghold2.exe"
#define POINTER_OFFSET 0x2A0F69
#define NEW_OPCODE 0x90909090909090

long getshpid(void);
long get_opcode_address(long);
void overwrite_opcode(long, long);

#endif /* OS_WRAPPER_H */
